import React, { useState, useEffect } from 'react';
import { Alert, View, ScrollView, Text, Image, StyleSheet, } from 'react-native';
import { Card } from '@rneui/themed';
import styles from './style.js'

function PokemonInfo({ route }) {

  const capitalizeFirstLetter = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  return (
    <View style={styles.container}>
      <Card style={styles.cardContainer}>
          <View>
            <Card.Title style={styles.cardTitle}>{capitalizeFirstLetter(route.params.nome)}</Card.Title>
            <Card.Divider />
            <Image resizeMode="stretch" source={{ uri: route.params.img }} style={styles.pokemonImg} />
            <Card.Divider />
            <Text style={styles.cardSection}>Informações</Text>
            <View>
              <Text style={styles.pokemonInfo}>Peso: {route.params.peso / 10} Kg.</Text>
              <Text style={styles.pokemonInfo}>Altura: {route.params.altura / 10} m.</Text>
            </View>
          </View>
      </Card>
    </View>
  );
}
export default PokemonInfo;