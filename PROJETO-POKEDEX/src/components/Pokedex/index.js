import React, { useState, useEffect } from 'react';
import { Alert, View, ScrollView, TextInput, FlatList, Text } from 'react-native';
import { Button, Card, SearchBar } from '@rneui/themed';
import styles from './style.js';

export default function Pokedex({ navigation }) {
  const [pokemonsIniciais, setPokemonsIniciais] = useState(null)
  const [pokemonEscolhido, setPokemonEscolhido] = useState(null);
  const [searchText, setSearchText] = useState('');
  const [filteredPokemon, setFilteredPokemon] = useState(pokemonsIniciais);

  const capitalizeFirstLetter = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  const handleSearch = (text) => {
    setSearchText(text);
    const filteredItems = pokemonsIniciais.filter((item) =>
      item.nome.toLowerCase().includes(text.toLowerCase())
    );
    setFilteredPokemon(filteredItems);
  };

  // Recebendo os dados de um Pokemon específico
  const getPokemonData = (idPokemon) => {
    const endpoint = 'https://pokeapi.co/api/v2/pokemon/' + idPokemon + '/';
    fetch(endpoint)
      .then(resposta => resposta.json())
      .then(json => {
        const pokemon = {
          id: idPokemon,
          nome: json.name,
          img: json.sprites.other["official-artwork"].front_default,
          peso: json.weight,
          altura: json.height
        };
        setPokemonEscolhido(pokemon);
      })
      .catch(() => {
        Alert.alert('Erro', 'Não foi possível carregar os dados do Pokémon');
      });
  };

  const getAllPokemon = () => {
    const endpoint = 'https://pokeapi.co/api/v2/pokemon?limit=1010&offset=0';
    fetch(endpoint)
      .then(resposta => resposta.json())
      .then(json => {
        const pokemonList = json.results.map((pokemon, index) => ({
          id: index + 1,
          nome: pokemon.name
        }));
        setPokemonsIniciais(pokemonList);
      })
      .catch(() => {
        Alert.alert('Erro', 'Não foi possível carregar os dados dos Pokémon');
      });
  };

  useEffect(() => {
    if (pokemonEscolhido !== null) {
      navigation.navigate('Dados do Pokémon', {
        id: pokemonEscolhido.id,
        peso: pokemonEscolhido.peso,
        altura: pokemonEscolhido.altura,
        nome: pokemonEscolhido.nome,
        img: pokemonEscolhido.img
      });
    }
  }, [pokemonEscolhido, navigation]);

  useEffect(() => {
    getAllPokemon();
  }, []);


  return (
    <View style={styles.container}>
      <View style={styles.cardContainer}>
        <SearchBar
          lightTheme='1'
          placeholder="Digite sua pesquisa..."
          onChangeText={handleSearch}
          value={searchText}
        />
        {(filteredPokemon !== null) && (
          <FlatList style={styles.cardSection}
            data={filteredPokemon.slice(0, 10)}
            renderItem={({ item }) => (
              <Card  key={item.id}>
                <View>
                  <Text style={styles.cardTitle}>{capitalizeFirstLetter(item.nome)}</Text>
                  <Button
                    title="Dados do Pokémon"
                    onPress={() => getPokemonData(item.id)}
                    type="outline"
                  />
                </View>
              </Card>
            )}
            keyExtractor={(item) => item.id.toString()}
            initialNumToRender={10}
          />)}
        {(filteredPokemon == null) && (
          <Text style={styles.textPesquisar}>Pesquise um Pokémon para iniciar!</Text>
        )}
      </View>
    </View>
  );
}
