import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: { flex: 1, marginBottom: 40 },
    textPesquisar: { textAlign: "center", marginTop: 10, marginBottom: 10, color: '#656565' },
    cardContainer: { height: 'auto', borderWidth: 1, borderColor: '#d5d5d5', borderRadius: 4, marginTop: 20, marginBottom: 40, marginHorizontal: 20 },
    cardTitle: { fontSize: 22, marginBottom: 20, textAlign: 'center', color: '#656565' },
    cardSection: { height: '100%'},
    pokemonBox: { paddingBottom: 100 },
    pokemonNome: { fontSize: 22 },
    pokemonPeso: { fontSize: 18 },
    pokemonImg: { width: 150, height: 150 }
})

export default styles