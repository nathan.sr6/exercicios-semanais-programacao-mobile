import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Pokedex from './src/components/Pokedex';
import PokemonInfo from './src/components/PokemonInfo';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          options={{
            headerTitle: () => (
              <Image
                source={require('./assets/pokemon-logo.png')} // Insira o caminho da imagem aqui
                style={{ width: 200, height: 90, marginTop: 20 }} // Defina o tamanho da imagem conforme necessário
              />  
            ),
            headerTitleAlign: 'center',
          }}
          name="Pokedex" component={Pokedex} />
        <Stack.Screen name="Dados do Pokémon" component={PokemonInfo} />
      </Stack.Navigator>
    </NavigationContainer >
  );
}
