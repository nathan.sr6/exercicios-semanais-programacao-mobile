import { StatusBar } from "expo-status-bar";
import React from "react"
import {View, Text, StyleSheet, SafeAreaView, FlatList, Button} from "react-native"

const DATA = [
    {id: '01', title: 'Elemento 1'},
    {id: '02', title: 'Elemento 2'},
    {id: '03', title: 'Elemento 3'},
];

const Item = ({title}) => (
    <View style={style.item}>
        <Text style={style.title}>
            {title}
        </ Text>
    </View>
)

function Galeria(){
    return(
        <SafeAreaView style={style.container}>
            <FlatList
                data={DATA}
                renderItem={({item}) => <Item title={item.title}/>}
                keyExtractor={item => item.id}
            />
        </SafeAreaView>
    );
}

export default Galeria;

const style = StyleSheet.create({
    container: {
        flex: 1, 
        alignItems: 'center', 
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 6,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32
    } 
})