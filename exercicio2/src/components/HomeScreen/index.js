import React from "react"
import {View, Text, StyleSheet, SafeAreaView, FlatList, Button} from "react-native"

function HomeScreen({navigation}){
    return(
        <View style={style.mainView}>
            <Text>Olá usuário!</Text>
            <Button 
                onPress={ () => navigation.navigate('Galeria')}
                title="Ir para a galeria!" />
            <Button 
                onPress={ () => navigation.navigate('Cálculo IMC')}
                title="Ir para cálculo IMC" />
        </View>
    );
}

export default HomeScreen;

const style = StyleSheet.create({
    mainView: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
        padding: 10,
    }
})