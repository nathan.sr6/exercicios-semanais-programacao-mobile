import React, {useState}  from "react"
import {View, Text, TextInput, Button} from "react-native"
import ResultImc from './ResultImc';

export default function Form(){
    const [height, setHeight] = useState(null)
    const [weight, setWeight] = useState(null)
    const [messageImc, setMessageImc] = useState("Preencha os dados...")
    const [imc, setImc] = useState(null)
    const [textButton, setTextButton] = useState("Calcular")

    function imcCalculator(){
        return setImc((weight/(height*height)).toFixed(2))
    }

    function validationImc(){
        if(weight != null && height != null){
            imcCalculator()
            setMessageImc("Resultado IMC: ")
            setTextButton("Calcular Novamente")
            return
        }
        setImc(null)
        setTextButton("Calcular")
        setMessageImc("Preencha os dados...")
    }

    return(
        //conteudo do formulario
        <View>
            <View>
                <Text>Altura:</Text><TextInput keyboardType="numeric" onChangeText={text => setHeight(text)}></TextInput>
            </View>
            <Text>Peso: </Text><TextInput keyboardType="numeric" onChangeText={text => setWeight(text)}></TextInput>
                <Button title={textButton} onPress={validationImc}/>
            <ResultImc messageResultImc={messageImc} resultImc={imc}></ResultImc>
        </View>
    );    
}
