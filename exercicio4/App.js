import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Main from './src/components/Main';
import HomeScreen from './src/components/HomeScreen';
import Galeria from './src/components/Galeria';
import Pokedex from './src/components/Pokedex';
import PokemonInfo from './src/components/PokemonInfo';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Galeria" component={Galeria} />
        <Stack.Screen name="Cálculo IMC" component={Main} />
        <Stack.Screen name="Escolha seu Pokemon!" component={Pokedex} />
        <Stack.Screen name="Dados do Pokémon" component={PokemonInfo} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1515',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
