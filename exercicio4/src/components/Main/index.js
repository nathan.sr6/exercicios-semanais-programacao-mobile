import React from "react"
import {View, Text, StyleSheet, StatusBar} from "react-native"
import Form from '../Form';

function Main(){
    return(
        <View style={style.mainView}>
            <Form />
        </View>
    );
};

export default Main;

const style = StyleSheet.create({
    mainView: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0, 
        padding: 10,
    }
})