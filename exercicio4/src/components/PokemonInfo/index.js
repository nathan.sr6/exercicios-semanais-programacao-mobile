import React, { useState, useEffect } from 'react';
import { Alert, View, ScrollView, Text, Image, StyleSheet, } from 'react-native';
import { Card } from '@rneui/themed';
import styles from './style.js'

function PokemonInfo({ route }) {
  const [pokemonEscolhido, setPokemonEscolhido] = useState(null);

  // Recebendo informações sobre cada pokemon
  const getPokemonData = (idPokemon) => {
    const endpoint = 'https://pokeapi.co/api/v2/pokemon/' + idPokemon + '/';
    fetch(endpoint)
      .then(resposta => resposta.json())
      .then(json => {
        const pokemon = {
          nome: json.name,
          img: json.sprites.other["official-artwork"].front_default,
          peso: json.weight,
          altura: json.height
        };

        setPokemonEscolhido(pokemon);
      })
      .catch(() => {
        Alert.alert('Erro', 'Não foi possível carregar os dados do Pokémon');
      });
  }

  const capitalizeFirstLetter = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  useEffect(() => {
    getPokemonData(route.params.id);
  }, []);

  return (
    <View style={styles.container}>
      <Card style={styles.cardContainer}>
        {pokemonEscolhido && (
          <View>
            <Card.Title style={styles.cardTitle}>{capitalizeFirstLetter(pokemonEscolhido.nome)}</Card.Title>
            <Card.Divider />
            <Image resizeMode="stretch" source={{ uri: pokemonEscolhido.img }} style={styles.pokemonImg} />
            <Card.Divider />
            <Text style={styles.cardSection}>Informações</Text>
            <View>
              <Text style={styles.pokemonInfo}>Peso: {pokemonEscolhido.peso / 10} Kg.</Text>
              <Text style={styles.pokemonInfo}>Altura: {pokemonEscolhido.altura} cm.</Text>
            </View>
          </View>
        )}
        {!pokemonEscolhido && (
          <View>
            <Text>Carregando...</Text>
          </View>
        )}

      </Card>
    </View>
  );
}

export default PokemonInfo;