import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: { flex: 1},
    cardContainer: { margin: 'auto', borderWidth: 1, borderColor: '#d5d5d5', borderRadius: 4, marginTop: 20, marginBottom: 20, marginHorizontal: 20, padding: 10 },
    cardTitle: { fontSize: 22, marginBottom: 20, textAlign: 'center', color: '#656565' },
    cardSection: { fontSize: 18, marginBottom: 20, textAlign: 'center', color: '#656565' },
    pokemonNome: { alignItems:'center', fontSize: 22 },
    pokemonInfo: { fontSize: 18 },
    pokemonImg: { alignSelf:'center', width: 150, height: 150 }
})

export default styles