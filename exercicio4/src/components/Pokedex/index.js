import React, { useState } from 'react';
import { Alert, View, ScrollView, Text, Image, } from 'react-native';
import { Button, Card } from '@rneui/themed';
import styles from './style.js'

const pokemonsIniciais = [
  { id: 1, nome: "Bulbasauro" },
  { id: 2, nome: "Ivysauro" },
  { id: 3, nome: "Venosauro" },
  { id: 4, nome: "Charmander" },
  { id: 5, nome: "Charmaleon" },
  { id: 6, nome: "Charizard" },
  { id: 7, nome: "Squirtle" },
  { id: 8, nome: "Wartotle" },
  { id: 9, nome: "Blastoise" },
  { id: 10, nome: "Caterpie" },
];

export default function Pokedex({navigation}) {  

  return (
    <View style={styles.container}>
      <ScrollView>
        {pokemonsIniciais.map(pokemon => (
          <Card>
            {
              <View>
                <Text style={styles.cardTitle}>{pokemon.nome}</Text>
                <Button title="Dados do Pokémon" onPress={() => navigation.navigate('Dados do Pokémon', {id: pokemon.id})} type="outline" />
              </View>
            }
          </Card>
        ))}

      </ScrollView>
    </View >
  );
}