import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: { flex: 1 },
    cardContainer: { borderWidth: 1, borderColor: '#d5d5d5', borderRadius: 4, marginTop: 20, marginBottom: 20, marginHorizontal: 20, padding: 10 },
    cardTitle: { fontSize: 22, marginBottom: 20, textAlign: 'center', color: '#656565' },
    cardSection: { fontSize: 20, marginBottom: 20, textAlign: 'center', color: '#656565' },
    pokemonBox: { alignItems: 'center', marginBottom: 50, borderWidth: 1, borderColor: '#d5d5d5', borderRadius: 4, paddingVertical: 20 },
    pokemonNome: { fontSize: 22 },
    pokemonPeso: { fontSize: 18 },
    pokemonImg: { width: 150, height: 150, }
})

export default styles